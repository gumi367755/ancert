#!/bin/bash

cmd=$1

if [ "$cmd" == "smartctl" ]; then
	cmd_prefix="smartctl -x"
elif [ "$cmd" == "hdparm" ]; then
	cmd_prefix="hdparm -I"
else
	echo "does not support command $cmd"
	exit 1
fi

echo "[root@localhost]# lsblk -nido NAME"
output=`lsblk -nido NAME`
echo "$output"

echo "$output" | while read line; do
	echo;echo;echo;
	dev_path="/dev/$line"
	echo "[root@localhost]# $cmd_prefix $dev_path"
	$cmd_prefix $dev_path
done
