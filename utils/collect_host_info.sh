#!/bin/bash

log_dir_path="$1"

function run_cmd() {
	local the_cmd="$1"
        local filename=$2
        echo "$the_cmd"
	echo "[root@localhost]# $the_cmd" > ${log_dir_path}/$filename
	eval "$the_cmd" >> ${log_dir_path}/$filename 2>&1
	echo;echo;echo
}

echo "collect udev information:"
run_cmd "udevadm info --export-db" "udevadm_info_export_db"

echo "collect DMI information:"
run_cmd "dmidecode" "dmidecode"

echo "collect cpu information:"
run_cmd "cat /proc/cpuinfo" "cat_proc_cpuinfo"

echo "collect pci information:"
run_cmd "lspci" "lspci"

echo "collect pci verbose information:"
run_cmd "lspci -vvvv" "lspci_vvvvv"

echo "collect block information:"
run_cmd "fdisk -l" "fdisk_l"

echo "collect usb information:"
run_cmd "lsusb -v" "lsusb_v"

echo "collect ip information:"
run_cmd "ifconfig -a" "ifconfig_a"

echo "collect pvs information:"
run_cmd "pvdisplay" "pvdisplay"

echo "collect vgs information:"
run_cmd "vgdisplay" "vgdisplay"

echo "collect lvs information:"
run_cmd "lvdisplay" "lvdisplay"

echo "collect lsblk information"
run_cmd "lsblk -io NAME,KNAME,MAJ:MIN,FSTYPE,MOUNTPOINT,LABEL,UUID,PARTLABEL,PARTUUID,RA,RO,RM,MODEL,SERIAL,SIZE,STATE,ALIGNMENT,MIN-IO,OPT-IO,PHY-SEC,LOG-SEC,ROTA,SCHED,RQ-SIZE,TYPE,WWN,PKNAME,HCTL,TRAN,REV,VENDOR" "lsblk_io"

echo "collect ethernet information"
run_cmd "bash utils/get_net_info.sh" "ethtool"

echo "collect mount information"
run_cmd "mount" "mount"

echo "collect df information"
run_cmd "df -P" "df_P"

echo "collect disk smart information"
run_cmd "bash utils/get_disk_info.sh smartctl" "smartctl_x_dev"

echo "collect disk hdparm information"
run_cmd "bash utils/get_disk_info.sh hdparm" "hdparm_I_dev"

echo "collect /proc/scsi/scsi information"
run_cmd "cat /proc/scsi/scsi" "proc_scsi_scsi"

echo "collect dmesg information"
run_cmd "dmesg" "dmesg"

echo "collect partitions information"
run_cmd "cat /proc/partitions" "cat_partitions"

echo "collect lsmod information"
run_cmd "lsmod" "lsmod"

echo "collect memory information"
run_cmd "cat /proc/meminfo" "cat_proc_meminfo"