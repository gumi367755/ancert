%define anolis_release 1
%define __lib lib
%undefine __brp_mangle_shebangs

Name:               ancert
Version:            2.0
Release:            %{anolis_release}%{?dist}
Summary:            Anolis Hardware Compatibility Test Suite
License:            MulanPSL2 and BSD
Group:              Development/Libraries
BuildRoot:          %{_tmppath}/%{name}-%{version}-%{release}-buildroot
Vendor:             Alibaba
URL:                https://gitee.com/anolis/ancert
Source:             %{name}-v%{version}.tar.gz
BuildRequires:      make, gcc, gcc-c++
Requires:           make, gcc, gcc-c++
Requires:           fio, nvme-cli, glx-utils, python3, rpm-build, bc, lvm2
Requires:           alsa-lib, alsa-utils, virt-what, smartmontools, hdparm
Requires:           xorg-x11-utils, xorg-x11-server-utils, xorg-x11-apps
Requires:           OpenIPMI, ipmitool, freeipmi
Requires:           iproute, iputils, ethtool, iperf3, sysstat, stress-ng, expect, tree
%if %(rpm -E %{?dist}|awk '{sub(".an","");print}') >= 8
Requires:           python3-pyyaml, clang
Requires:           memstrack
%endif
Requires:           mdadm
ExclusiveArch:      x86_64 aarch64 loongarch64

%description
This test suite to be used to verify your system or component the compatibility with Anolis OS.

%prep
%setup -qn %{name}-v%{version}

%install
make DESTDIR=%{buildroot}%{_datadir}/%{name} post_install

%files
%defattr(-,root,root)
%{_datadir}/%{name}/%{name}
%{_datadir}/%{name}/etc
%{_datadir}/%{name}/%{__lib}
%{_datadir}/%{name}/tests
%{_datadir}/%{name}/utils
%{_datadir}/%{name}/README.md
%{_datadir}/%{name}/LICENSE
%exclude %{_datadir}/%{name}/%{name}.spec

%post
if [ "$1" = "1" ]; then
    [ -f %{_bindir}/%{name} ] || [ -L %{_bindir}/%{name} ] \
        && rm -rf %{_bindir}/%{name}
    ln -s %{_datadir}/%{name}/%{name} %{_bindir}/%{name}
fi

%postun
if [ "$1" = "0" ]; then
    [ -d %{_datadir}/%{name} ] && rm -rf %{_datadir}/%{name}
    [ -L %{_bindir}/%{name} ] && rm -rf %{_bindir}/%{name}
fi

%clean
[ "$RPM_BUILD_ROOT" != "/" ] && [ -d $RPM_BUILD_ROOT ] && rm -rf $RPM_BUILD_ROOT;

%changelog
* Mon Nov 21 2022 chaofengwu <17269747_wcf@163.com> -2.0-1
- update ancert architecture to 2.0

* Tue Aug 16 2022 chaofengwu <17269747_wcf@163.com> -1.2-1
- support IPMI , BIOS test

* Thu Mar 3 2022 chaofengwu <17269747_wcf@163.com> -1.1-1
- fixed several bugs

* Wed Nov 24 2021 chaofengwu <17269747_wcf@163.com> -1.0-1
- support log packaging and test results checksum