#!/usr/bin/env bash


function common_env() {
    FSTAB_CONF="/etc/fstab"
    SYS_BLOCK="/sys/block"
    PCI_SLOTS="/sys/bus/pci/slots"
    ESSH="ssh -o StrictHostKeyChecking=no"
    CPU_PATH="/sys/devices/system/cpu"
    CPU_LOGDIR="`mkdir -p /tmp/cpu && ls -d /tmp/cpu/`"
    MEM_LOGDIR="`mkdir -p /tmp/memory && ls -d /tmp/memory/`"
}

function stress_cpu_env() {
    CPU_TEST_TYPE=${1:-"unknown"}
    CPU_RUNTIME=${2:-60}
    CPU_STRESS_TEST_LOG=${3:-"${CPU_LOGDIR}`date +%F-%H-%M-%S`_cputest.log"}
    CPU_ONLINE_NUMS=0
    CPU_ONLINE_NAME=""
    CPU_BURN_LOG="${CPU_LOGDIR}`date +%F-%H-%M-%S`_cpuburn.log"
    CPU_PI_TEST_LOG="${CPU_LOGDIR}`date +%F-%H-%M-%S`_cpu_pi.log"
    CPU_MPRIME_LOG="${CPU_LOGDIR}`date +%F-%H-%M-%S`_mprime.log"
    CPU_LOAD_LOG="${CPU_LOGDIR}`date +%F-%H-%M-%S`_cpuload.log"
    CPU_LOAD_RESULTS_LOG="${CPU_LOGDIR}`date +%F-%H-%M-%S`_cpuload_res.log"
    START_CPU_LOAD=
    CUR_CPU_LOAD=
}

function stress_memory_env() {
    CUR_USED_MEM=
    MEM_SIZE_MB=
    MEM_LOG="${MEM_LOGDIR}`date +%F-%H-%M-%S`_memory.log"
    MEM_STATE="${MEM_LOGDIR}`date +%F-%H-%M-%S`_memory_state.log"
}

function stress_nvme_env() {
    #fio
    FIO_NUMJOBS=${1:-1}
    FIO_RUNTIME=${2:-120}
    FIO_SIZE=${3:-"128M"}
    FIO_MODE=${4:-"randwrite"}
    FIO_MIX=${5:-0}
    FIO_NVME_LOGPATH=$(mkdir -p /tmp/nvme_fio && ls -d /tmp/nvme_fio)
    FIO_STATUS_LOG="${FIO_NVME_LOGPATH}/nvme_fio_${FIO_MODE}.status"
    DEFAULT_SPEED=100
}

function stress_network_env() {
    IPERF3_PROCESS_NUM=${1:-1}
    IPERF3_RUNTIME=${2:-10}
    IPERF3_TRANSMIT_RATE=${3:-"10M"}
    IPERF3_TRANSMIT_SIZE=${4:-"128K"}
    IPERF3_CONNECT_MODE=${5:-"TCP"} #TCP or UDP
    IPERF3_LOG=""
    IPERF3_PORT=5201
    IPERF3_INTERVAL=0
    IPERF3_LOG_PATH=$(mkdir -p /tmp/network && ls -d /tmp/network)
    NETWORK_DRIVER_INFO=
    NET_FACE_STATUS=
}

common_env
