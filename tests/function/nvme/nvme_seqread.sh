#!/usr/bin/env bash

source ../../lib/shell/common.sh

IS_CLEANUP=

function check() {
    for cmd in {numactl,nvme,fio};do
        check_cmd "$cmd"
    done
    nvme_dev_check
}

function nvme_seqread_setup() {
    fio_setup filename="$DEVNAME" rw="read" name="nvme_seqread"
    get_nvme_free_disk || { echo "Failed to get nvme free disk";exit 1; }

    if ! echo "${NVME_FREE_DISK[*]}"|grep -wq "$DEVNAME";then
        write_messages warn "Not clean up $DEVNAME for nvme fio test!"
        IS_CLEANUP="false"
    else
        IS_CLEANUP="true"
    fi
}

function nvme_seqread_test() {
    # The write operation comes before the read operation
    fio_setup filename="$DEVNAME" rw="write" name="nvme_seqwrite"
    eval "numactl -N 0 ${FIO_CMD}"

    nvme_seqread_setup
    eval "numactl -N 0 ${FIO_CMD}"
    if [ $? -ne 0 ]; then
        write_messages err "====================nvme dev fio test seq read for ${DEVNAME} failed===================="
        test_fail
    fi
    write_messages info "====================nvme dev fio test seq read for ${DEVNAME} finish.===================="
}

trap '
if "$IS_CLEANUP";then
    clean_up_disk_partition "$DEVNAME"
fi
' EXIT
# # # # # # # # # # # # # # main # # # # # # # # # # # # # #
check
nvme_seqread_test
test_pass
