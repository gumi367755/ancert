#!/bin/bash

source ../../lib/shell/common.sh

# # # # # # # # # # # # # # main # # # # # # # # # # # # # #
echo "kernel version test"

cur_kernel=`uname -r`
rpm_inq_ret=$(rpm -qa | grep kernel-[0-9])
uname_r_ret="kernel-`uname -r`"
rpm_inq_kernel_cnt=$(rpm -qa | grep kernel-[0-9] | wc -l)

if [ $rpm_inq_kernel_cnt -gt 1 ];then
    write_messages warn "There are multiple kernel pkg, please confirm ${cur_kernel} whether it is currently valid"
elif [ "$rpm_inq_ret" != "$uname_r_ret" ];then
    write_messages err "uname -r: $uname_r_ret is different with rpm pkg: $rpm_inq_ret."
    test_fail
else
    write_messages info "uname -r: $uname_r_ret is same with rpm pkg: $rpm_inq_ret, kernel version check pass."
fi

test_pass
