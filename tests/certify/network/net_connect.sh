#!/usr/bin/env bash

source ../../lib/shell/common.sh

function setup() {
	echo "check test tool rpm before test"
	ip link help vlan&> /dev/null || { echo "iproute is not installed";exit 1; }
}

function connectcheck() {
	echo "run network connect test!"
	ifconfig
	lts_ip=$LTS_IPADDRESS
	echo "test lts ip info:$lts_ip"
	ping -c 4 "$lts_ip"
	if [ $? -eq 0 ]; then
		echo "test network ping function success"
		return 0
	else
		echo "test network ping function failed"
		return 1
	fi
}

function run_network_test() {
	setup
	connectcheck
	if  [ $? -ne 0 ]; then
		test_fail
	fi
}

# # # # # # # # # # # # # # main # # # # # # # # # # # # # #
print_test_info "Network"
show_network_info
run_network_test
test_pass

