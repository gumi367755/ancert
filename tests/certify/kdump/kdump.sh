#!/usr/bin/env bash
source ../../lib/shell/common.sh

COREDIR=/var/crash
FLAGFILE='crash.txt'
BASE_PATH="$(cd "$(dirname "$0")";pwd)"
VMLINUX_PATH=/lib/debug/lib/modules/`uname -r`/vmlinux

function setup_kdump() {
	if [ ! -x "/sbin/kexec" ]; then
		echo "Fail: kexec-tools not found."
		error=1
	fi

	if ! grep 'crashkernel=' /proc/cmdline; then
		echo "Fail: error changing Boot Loader, no crashkernel=."
		error=1
	fi

	debuginfo_load=$(rpm -qa | grep "debuginfo")
	if [[ $debuginfo_load =~ "kernel-debuginfo" ]] && [[ $debuginfo_load =~ "kernel-debuginfo-common" ]]; then
		echo "kernel-debuginfo and kernel-debuginfo-common have loaded!"
	else
		echo "Fail: kernel-debuginfo and kernel-debuginfo-common should be loaded first!"
		error=1
	fi

	echo $VMLINUX_PATH
	if [ -a $VMLINUX_PATH ]; then
		echo "vmlinux file is exist!"
	else
		echo "Fail: vmlinux file is not exist, path is $VMLINUX_PATH"
		error=1
	fi

	if [ "${error}" ]; then
		echo "Please fixed the above failures before continuing."
		return 1
	else
		echo "kdump prepare check success!"
	fi

	cat /etc/default/grub
	cat /etc/kdump.conf
	#kdump enable
	ret="$(systemctl status kdump)"
	echo $ret
	active_str='Active: active'
	if ! [[ $ret =~ $active_str ]]; then
		echo "kdump is not enabled, start enable kdump"
		systemctl start kdump.service
		for ((i=0;i<5;i++))
		do
			sleep 10
			ret="$(systemctl status kdump)"
			if [[ $ret =~ $active_str ]]; then
				break
			fi
		done
		if [ $i -eq 5 ]; then
			echo "Fail: error to  enable kdump"
			return 1
		fi
	fi
	echo "kdump enabled success"

	#Restart automatically excute ancert kdump check
	conffile='/etc/rc.d/rc.local'
	find_ret=$(find $BASE_PATH/../../ -name "ancert")
	if [ $find_ret ]; then
		python_excu='python3'
	else
		python_excu=''
	fi
	cmd1="cd $BASE_PATH/../../ "
	cmd2="$python_excu ancert -g Kdump"
	echo $cmd1
	echo $cmd2
	sed -i "\$a $cmd1" $conffile
	sed -i "\$a $cmd2" $conffile
	chmod +x $conffile

	return 0
}

function run_kdump_test() {
	setup_kdump
	if [ $? -ne 0 ]; then
		echo "setup kdump fail"
		test_fail
	fi

	echo "start to trigger crash"
	rm -rf $COREDIR
	systemctl status kdump
	echo "kdump test finish"> $FLAGFILE
	sync
	echo c > /proc/sysrq-trigger
	return 0
}

function run_vmcore_check() {
	if [ "$(ls -A $COREDIR)" = "" ]; then
		echo $COREDIR" is empty, kdump test fail!"
		test_clean
		test_fail
	else
		echo "vmcore folder is exist, start to run crash cmd!"
		rm -f crash.log
		echo q | crash $VMLINUX_PATH $COREDIR/`ls $COREDIR`/vmcore > crash.log
		if [ $? -eq 1 ]; then
			echo "crash cmd fail, kdump test fail!"
			cat crash.log
			test_clean
			test_fail
		else
			cat crash.log
			echo "kdump test success!"
			return 0
		fi
	fi
}

function test_clean() {
	echo "test finish, start to recover conf"
	rm -f $FLAGFILE
	conffile='/etc/rc.d/rc.local'
	sed -i "\$d" $conffile
	sed -i "\$d" $conffile
}

# # # # # # # # # # # # # # main # # # # # # # # # # # # # #
print_test_info "Kdump"
if [ -e $FLAGFILE ]; then
	run_vmcore_check
	test_clean
else
	run_kdump_test
fi
test_pass
