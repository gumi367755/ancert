#!/usr/bin/env bash

source ../../lib/shell/common.sh

function get_memory_info() {
	echo "get memory information:"
	dmidecode -t memory
}

function run_memory_test() {
	gcc --version &> /dev/null || { echo "gcc is not installed";exit 1; }
	make
	./memory
	if [ $? -ne 0 ]; then
		test_fail
	fi
	make clean
}

# # # # # # # # # # # # # # main # # # # # # # # # # # # # #
print_test_info "Memory"
get_memory_info
run_memory_test
test_pass
