#include "hip/hip_runtime.h"
#include <hip/hip_runtime.h>
#include <math.h>

#define N 500000

__global__ void add(float *d_A, float *d_B, float *d_C) {
    int tid = threadIdx.x + blockIdx.x * blockDim.x;
    if (tid < N) {
        d_C[tid] = d_A[tid] + d_B[tid];
    }
}

int main() {
    //申请数据空间
    printf("[Vector addition of %d elements]\n\n", N);
    float *A = (float *) malloc(N * sizeof(float));
    float *B = (float *) malloc(N * sizeof(float));
    float *C = (float *) malloc(N * sizeof(float));
    float *d_A = NULL;
    float *d_B = NULL;
    float *d_C = NULL;
    hipMalloc((void **) &d_A, N * sizeof(float));
    hipMalloc((void **) &d_B, N * sizeof(float));
    hipMalloc((void **) &d_C, N * sizeof(float));
    //数据初始化
    printf("Initialize data with constants data\n");
    for (int i = 0; i < N; i++) {
        A[i] = 1;
        B[i] = 1;
        C[i] = 0;
    }
    //数据从系统拷贝到GPU
    printf("copy data from host to device gpu\n");
    hipMemcpy(d_A, A, sizeof(float) * N, hipMemcpyHostToDevice);
    hipMemcpy(d_B, B, sizeof(float) * N, hipMemcpyHostToDevice);
    hipMemcpy(d_C, C, sizeof(float) * N, hipMemcpyHostToDevice);
    int block = 256;
    int grid = N/block + 1;
    dim3 blocksize(block, 1);
    dim3 gridsize(grid, 1);
    // 进行数组相加
    printf("do vector add operation with %d blocks of %d threads\n", block, grid);

    add<<<gridsize, blocksize >>> (d_A, d_B, d_C);

    printf("Copy results from gpu to host\n\n");
    hipMemcpy(C, d_C, sizeof(float) * N, hipMemcpyDeviceToHost);
    
    //结果验证

    for (int i = 0; i < N; i++) {
        if (abs(A[i] + B[i] - C[i]) > 1e-5) {
            printf("Result verification failed at element %d!\n", i);
            printf("Test failed\n");
            exit(1);
   
	}
    }
    //释放申请空间
    printf("Test passed!\n");
    free(A);
    free(B);
    free(C);
    hipFree(d_A);
    hipFree(d_B);
    hipFree(d_C);
}
