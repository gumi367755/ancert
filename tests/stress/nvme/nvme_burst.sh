#!/usr/bin/env bash

source ../../lib/shell/env.sh
source ./nvme_fio.sh

function burst(){
    echo "nvme burst test start"
    for ((i=1;i<=5;i++));do
        for num in {10,1000};do
            FIO_NUMJOBS=$num
            echo -e "\n----------Number of fio threads[$num]----------"
            nvme_fio_test
            [ $? -ne 0 ] && test_fail "failed to nvme burst test"
        done
    done
}

# # # # # # # # main # # # # # # # #
stress_nvme_env 5 $(echo "$RUNTIME / 5 / 2 "|bc)

cur_time_diff=0
for ((i=1;i<=$RUNTIME;i+=$cur_time_diff));do
    cur_time=$(date +%s -d "$(date "+%F %H:%M:%S")")
    echo -e "\n[NVMe fio burst test--->$i]"
    burst
    cur_time_diff=$(($(date +%s -d "$(date "+%F %H:%M:%S")") - $cur_time))
    [ $(($i + 2 * $cur_time_diff)) -ge $RUNTIME ] && break
done
test_pass
