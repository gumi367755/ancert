#!/usr/bin/env bash

source ../../lib/shell/env.sh
source ./nvme_fio.sh

stress_nvme_env 5 $RUNTIME "128M" "randwrite"

cur_time_diff=0
for ((i=1;i<=$RUNTIME;i+=$cur_time_diff));do
    cur_time=$(date +%s -d "$(date "+%F %H:%M:%S")")
    echo -e "\n[NVMe fio randwrite test--->$i]"
    nvme_fio_test || test_fail "Error: failed to nvme fio test"
    cur_time_diff=$(($(date +%s -d "$(date "+%F %H:%M:%S")") - $cur_time))
    [ $(($i + 2 * $cur_time_diff)) -ge $RUNTIME ] && break
done
test_pass