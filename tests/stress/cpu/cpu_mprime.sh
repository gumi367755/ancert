#!/usr/bin/env bash

source ../../lib/shell/env.sh
source ./cpu_stress.sh

BASEDIR="$(cd $(dirname $0);pwd)"

function clean() {
    echo "clean $CPU_MPRIME_LOG"
    [ -f $CPU_MPRIME_LOG ] && rm -rf $CPU_MPRIME_LOG
    echo "clean $CPU_STRESS_TEST_LOG"
    [ -f $CPU_STRESS_TEST_LOG ] && rm -rf $CPU_STRESS_TEST_LOG

    if pidof mprime >/dev/null 2>&1;then
        echo "kill mprime process"
        kill -9 `pidof mprime`
    fi
    echo "clean ${BASEDIR}/nohup.out"
    [ -f ${BASEDIR}/nohup.out ] && rm -rf ${BASEDIR}/nohup.out
    echo "clean ${BASEDIR}/*.txt"
    for f in ${BASEDIR}/*.txt;do
        test -f "$f" && rm -rf "$f"
    done
}

trap "clean" EXIT

# # # # # # # # # # # # # # main # # # # # # # # # # # #
stress_cpu_env "mprime" $RUNTIME

cpu_stress_test && test_pass || test_fail
