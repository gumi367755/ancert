#!/usr/bin/env bash

source ../../lib/shell/env.sh
source ./cpu_stress.sh

function exit_exec() {
    echo "cpu restore"
    for cpu in $CPU_ONLINE_NAME;do
        echo "echo 1 > ${CPU_PATH}/${cpu}/online"
        echo 1 > "${CPU_PATH}/${cpu}/online"
    done

    echo "clean $CPU_LOAD_LOG"
    test -f "$CPU_LOAD_LOG" && rm -rf "$CPU_LOAD_LOG" ||:
    echo "clean $CPU_LOAD_RESULTS_LOG"
    test -f "$CPU_LOAD_RESULTS_LOG" && rm -rf "$CPU_LOAD_RESULTS_LOG" ||:
    if pgrep stress-ng >/dev/null 2>&1;then
        echo "kill -9 \`pgrep stress-ng\`"
        kill -9 `pgrep stress-ng`
    else
        echo "Nothing to kill for cpu Computational power test."
    fi
}

function cpu_on_off() {
    local cur_time=0
    local cur_time_diff=0

    #wait stress-ng process start
    sleep 10

    for ((i=10;i<=$RUNTIME;i+=$cur_time_diff));do
        cur_time="$(date +%s -d "$(date +"%a %b %e %T %Y")")"
        #random 1 cpu
        cur_random="$(echo $RANDOM%$CPU_ONLINE_NUMS |bc)"
        printf "[RANDOM CPU]: cpu%-8s [Time consuming]: in %-8s [RUNTIME]: %s\n" "${cur_random}" "${i}s" "${RUNTIME}s"
        echo 0 > ${CPU_PATH}/cpu${cur_random}/online
        if [ $(($i + 30)) -ge $RUNTIME ];then
            sleep 3
            echo 1 > ${CPU_PATH}/cpu${cur_random}/online && break
        fi
        sleep 30
        echo 1 > ${CPU_PATH}/cpu${cur_random}/online
        cur_time_diff=$(($(date +%s -d "$(date +"%a %b %e %T %Y")") - $cur_time))
        [ $(($i + $cur_time_diff)) -ge $RUNTIME ] && break
    done
}

function cpu_on_off_stress_test() {
    #method: A cpu is randomly offline and keep 30s,
    #both cpu offline and monitor cpu load and cpu stress test as a background process.
    local fail_msg=""
    local cpuload_pid
    local cpu_used_nums

    #online cpu info, reserve 3 cpu
    echo -e "Get online cpu: \n=============================="
    for on_cpu in $CPU_ONLINE_NAME;do echo "${CPU_PATH}/$on_cpu";done
    echo -e "=============================="
    if test $CPU_ONLINE_NUMS -gt 0 && test $CPU_ONLINE_NUMS -lt 3;then
        echo "warning: The test conditions are not met,At least 3 cpu.(reserve 2 cpu, 1 cpu test)" && test_skip
    elif test $CPU_ONLINE_NUMS -ge 3;then
        cpu_used_nums=$(echo "$CPU_ONLINE_NUMS - 2"|bc)
    else
        echo "warning: No cpu is online." && test_skip
    fi

    check_cmd "stress-ng"
    cpuload_monitor_on_usr $cpu_used_nums "true" &
    cpu_on_off &
    run_cmd "stress-ng --cpu $cpu_used_nums --timeout=$RUNTIME" &
    wait

    echo "Check cpu load log"
    if test ! -f $CPU_LOAD_RESULTS_LOG;then
        echo "Error: no such file $CPU_LOAD_RESULTS_LOG" && test_fail
    else
        if grep -wq "Error" $CPU_LOAD_RESULTS_LOG;then
            awk '{print $0}' < $CPU_LOAD_RESULTS_LOG && test_fail
        fi
        awk '{print $0}' < $CPU_LOAD_RESULTS_LOG
    fi
}

trap "exit_exec" EXIT

# # # # # # # # # # # # # # main # # # # # # # # # # # #
stress_cpu_env
get_online_cpu || test_fail

cpu_on_off_stress_test && test_pass || test_fail
