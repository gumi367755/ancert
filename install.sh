#!/usr/bin/env bash

PKGS=
BASEDIR=$(cd `dirname $0`; pwd)

function get_all_depend_pkgs() {
    local _pkgs=""
    cd $BASEDIR
    for pkg in `cat ancert.spec|grep -w "Requires:"|awk -F ':' '{print $2}'`
    do
        if [[ "${pkg}" =~ "," ]];then
            _pkgs="${_pkgs} ${pkg}"
        else
            _pkgs="${_pkgs} ${pkg},"
        fi
    done
    PKGS=$(echo ${_pkgs}|awk '{gsub(/,/," "); print}')
    echo -e "Get all dependent package information:\n ${PKGS}"
}

function install_all_depend_pkgs() {
    get_all_depend_pkgs
    if test -f /etc/os-release;then
        if ! cat /etc/os-release|grep PRETTY_NAME|grep -iwq Anolis;then
            echo "Error: please install it in Anolis OS system!" && exit 1
        fi
    else
        echo "Error: no such file /etc/os-release, please install it in Anolis OS system!" && exit 1
    fi
    echo -e "\ninstall dependent package information:"
    for pkg in $PKGS;do
        cmd="yum install ${pkg} -y"
        echo -e "\033[1;40;32m\n${cmd}\033[0m"
        eval $cmd
        [ $? -eq 0 ] || exit 1
    done
}

function source_code_to_install() {
    cd $BASEDIR
    _ancert=$(rpmspec -P ancert.spec|egrep "Name"|awk '{print $NF}')
    [ X"${_ancert}" = X"" ] && echo "Failed to get Name from ancert.spec." && exit 1
    if rpm -qa|grep "$_ancert" >/dev/null 2>&1;then
        rpm -e $_ancert
        [ $? -eq 0 ] || echo -e "\nFaild to uninstall $_ancert, please execute the command: rpm -e $_ancert" && exit 1
    fi
    echo "make install."
    make install
    [ $? -eq 0 ] || echo -e "\nSource code installation failed, please execute the command:make uninstall && make install"
    [ -f "`rpm -E %{_datadir}`/${_ancert}/${_ancert}.spec" ] && rm -rf "`rpm -E %{_datadir}`/${_ancert}/${_ancert}.spec"
}

# # # # # # # # # # # # # # main # # # # # # # # # # # #
install_all_depend_pkgs
source_code_to_install