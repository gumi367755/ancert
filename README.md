## OpenAnolis Hardware Compatibility Test Suite
This test suite to be used to verify your system or component compatibility with anolis on x86,arm,loongarch platform

# Usage
```shell
$sudo python3 ancert -h
usage: ancert [-h]
              [-g {System,BIOS,CPU,Memory,Video,GPU,Storage,Network,NVMe,FC,IPMI,RAID,Kdump,Disk,Misc}]
              [-f FEATURE] [-c CASES [CASES ...]] [-i INDEX] [--lts_ip LTS_IP]
              [--exclude_interface EXCLUDE_INTERFACE] [--single_mode] [-r]
              [--tone] [--list_testcase]
              [--list_hardware {All,System,BIOS,CPU,Memory,Video,GPU,Storage,Network,NVMe,FC,IPMI,RAID,Kdump,Disk,Misc}]
              [-m {certify,function}]

ancert

optional arguments:
  -h, --help            show this help message and exit
  -g {System,BIOS,CPU,Memory,Video,GPU,Storage,Network,NVMe,FC,IPMI,RAID,Kdump,Disk,Misc}, --category {System,BIOS,CPU,Memory,Video,GPU,Storage,Network,NVMe,FC,IPMI,RAID,Kdump,Disk,Misc}
                        Category
  -f FEATURE, --feature FEATURE
                        Feature
  -c CASES [CASES ...], --cases CASES [CASES ...]
                        Cases
  -i INDEX, --index INDEX
                        Want to test device index
  --lts_ip LTS_IP       LTS ip address
  --exclude_interface EXCLUDE_INTERFACE
                        exclude interface name such as eth0
  --single_mode         Run System test without LTS
  -r, --report          Summary report
  --tone                Work with Tone
  --list_testcase       List all the testcases
  --list_hardware {All,System,BIOS,CPU,Memory,Video,GPU,Storage,Network,NVMe,FC,IPMI,RAID,Kdump,Disk,Misc}
                        List hardware on the system for target category
  -m {certify,function}, --mode {certify,function}
                        Currently supported test mode: certify, function

Example:
  python3 ancert -h
  python3 ancert -g Storage -c fs_rw raw_disk_rw
```
# How to compile ancert
```shell
Make sure you have the GUN Make tool installed on Anolis OS,If not installed,
The installation can be done through yum.
For example,
    $yum install make -y

You should be in the ancert repository root directory,Run the make command;but
Before executing the make command, you need to write the Makefile,How to write 
a makefile,Please refer to the https://www.gnu.org/software/make/manual/make.html#Makefiles,
please touch a Makefile,Let's look at a simple example about Makefile.
For example,
    .PHONY: clean install
    install:
        echo "install..."
    clean:
        echo "clean..."

If you want to build directly on ancert tests, you can expand Makefile on the following.
For example,
    .PHONY: clean install
    
    install:
        @if [ ! -z $(DESTDIR) ];then \
            { [ ! -d $(DESTDIR) ] && mkdir -p -m 755 $(DESTDIR); }; \
        else\
            echo "There is no target path \$$DESTDIR to operate on"; \
        fi   
    clean:
        @if [ ! -z $(DESTDIR) ];then \
            echo "rm -rf \$$(DESTDIR)/*........"; \    
        else\
            echo "There is no target path \$$DESTDIR to operate on"; \
        fi
    uninstall:
    	@echo "uninstall..."

Of course, you can rewrite the Makefile as you wish.

Next the test runs the makefile.
Run the make command in the ancert repository root directory.
if make first compile,Run the make command,Here's what you should do.
For example,
    $make install

if make multiple times in the same path,Run the make command,Here's what you should do.
For example,
    $make uninstall
    $make install

If the exit status is 0, the execution succeeds. Otherwise, the execution fails.
To obtain the command result, perform the following operations.
For example,
    $echo $?

Detailed usage reference for make,please make --help.
```
# How to build ancert rpm
```shell
You need to install rpm-build,make,gcc,gcc-c++ before building the RPM package on Anolis OS,
If not installed,The installation can be done through yum.
For example,
    $yum install -y make gcc gcc-c++ clang fio nvme-cli glx-utils python3 python3-pyyaml rpm-build bc lvm2 alsa-lib alsa-utils virt-what smartmontools hdparm xorg-x11-utils xorg-x11-server-utils xorg-x11-apps OpenIPMI ipmitool freeipmi numactl kpatch
    
Then run the build script directly to get the RPM,Run the script in the ancert repository root directory.
For example,
    $sh build_rpm.sh rpm

Obtain the RPM package from the following path.
For example,
    $cd /root/rpmbuild/RPMS/		
    $find ./ -name "*.rpm"

Detailed usage reference for rpmbuild,please rpmbuild --help.
```
# How to install ancert from source code
```shell
just run the following commands to install ancert from source code:
    $cd ancert
    $sh install.sh

after installation, you can find out the ancert files in /usr/share/ancert.
    $cd /usr/share/ancert
```

# Note
1. please run this test suite as root
2. require at least one free raw disk(>10G) for storage test


