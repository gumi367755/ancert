

class Case(object):
    __slots__ = ['component', 'filetype', 'filepath', 'description', 'runtime', 'name', 'device',
                 'group', 'required', 'feature']

    def __str__(self):
        return self.name
