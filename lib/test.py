import os
import datetime


class Test(object):
    def __init__(self, case, device, ctr, logdir, mode):
        self.cmd = ''
        self.case = case
        self.device = device
        self.controller = ctr
        self.device.properties.update({'PCI_BUS_DEVICE_FUNCTION': self.controller.bdf})
        self.device.properties.update({'DRIVER': self.controller.driver})
        self.device.properties.update({'MODULE': self.controller.module})
        self.cwd = '{0}/tests/{1}/{2}'.format(os.getcwd(), mode, self.case.component)
        self.env = dict(os.environ)
        self.env.update({'RUNTIME': '{}'.format(self.case.runtime)})
        if self.case.filetype == 'shell':
            self.env.update(self.device.properties)
            self.cmd = '%s %s' % ('/bin/bash', self.case.filepath)
        elif self.case.filetype == 'python':
            self.cmd = self.case.filepath.rsplit('.', 1)[0]
        self.name = '{0}_{1}_{2}'.format(self.case.component, self.case.group, self.case.name)
        self.result = 'FAIL'
        self.logdir = os.path.join(logdir, self.name)
        if not os.path.exists(self.logdir):
            os.makedirs(self.logdir)
        self.env.update({'TESTNAME': self.case.name})
        self.env.update({'TESTLOG_DIR': self.logdir})
