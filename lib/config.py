TESTS_DIR = 'tests'
LOG_BASE_DIR = '/var/log/ancert'
POOL_MAX_WORKERS = 10
ALL_DEVICE_INFO = 'info.json'
TOOL_NAME = 'ancert'
NO_DRIVER_STR = '* No Driver *'
